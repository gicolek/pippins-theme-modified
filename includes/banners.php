<?php
/**
 * Banners
 */

/**
 * AffiliateWP
 */
function pp_banner_affwp() {
?>
	<aside class="widget">
		<a href="https://affiliatewp.com" target="_blank">
			<img title="AffiliateWP" src="<?php echo get_stylesheet_directory_uri() . '/images/300x250-affwp.png'; ?>">
		</a>
	</aside>
<?php
}

/**
 * EDD
 */
function pp_banner_edd() {
?>
	<aside class="widget">
		<a href="https://easydigitaldownloads.com" target="_blank">
			<img title="Easy Digital Downloads" src="<?php echo get_stylesheet_directory_uri() . '/images/300x250-edd.png'; ?>" />
		</a>
	</aside>
<?php
}

